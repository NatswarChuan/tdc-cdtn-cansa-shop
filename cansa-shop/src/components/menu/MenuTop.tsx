import { Reorder } from '@material-ui/icons';
import { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './menutop.css';

export default function MenuTop() {
    const [displayMenu, setdisplayMenu] = useState(false)

    return (
        <>
            <div className="menu-container">
                <ul className="menu-list">
                    <li className="menu-list-item"><NavLink activeClassName="menu-link-selected" className="menu-link" exact to='/'>Trang chủ</NavLink></li>
                    <li className="menu-list-item"><NavLink activeClassName="menu-link-selected" className="menu-link" exact to='/categories'>Danh mục</NavLink></li>
                    <li className="menu-list-item"><NavLink activeClassName="menu-link-selected" className="menu-link" exact to='/cart'>Giỏ hàng</NavLink></li>
                    <li className="menu-list-item"><NavLink activeClassName="menu-link-selected" className="menu-link" exact to='/account'>Tài Khoản</NavLink></li>
                </ul>

                <div onClick={() => setdisplayMenu(true)} className="menu-phone-title"><Reorder style={{ marginRight: 5 }} /> Menu</div>

                <div onClick={() => setdisplayMenu(false)} className={displayMenu ? "menu-phone" : "menu-phone  menu-phone-hidden"} >
                    <div className="menu-phone-title"><Reorder style={{ marginRight: 5 }} /> Menu</div>
                    <div className="menu-list-item"><NavLink activeClassName="menu-phone-selected" className="menu-link" exact to='/'>Trang chủ</NavLink></div>
                    <div className="menu-list-item"><NavLink activeClassName="menu-phone-selected" className="menu-link" exact to='/categories'>Danh mục</NavLink></div>
                    <div className="menu-list-item"><NavLink activeClassName="menu-phone-selected" className="menu-link" exact to='/cart'>Giỏ hàng</NavLink></div>
                    <div className="menu-list-item"><NavLink activeClassName="menu-phone-selected" className="menu-link" exact to='/account'>Tài Khoản</NavLink></div>
                    <div onClick={() => setdisplayMenu(false)} className="menu-list-cancel">Huỷ</div>
                </div>
            </div>



            <div onClick={() => setdisplayMenu(false)} className={displayMenu ? "menu-phone-overlay" : "menu-phone-overlay  menu-phone-hidden"}  ></div>
        </>
    )
}
