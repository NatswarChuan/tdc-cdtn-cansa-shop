import './product.css'
import ReactStars from 'react-stars'
import { ProductModel } from '../../redux'
import { SlugStrTitle, TextToSlug, vnd } from '../../consts/Selector'
import { NavLink } from 'react-router-dom'

export default function Product(props: any) {
    const product: ProductModel = props.product;
    return (
        <NavLink to={`/product-detail/${TextToSlug(product.product_title)}/${product.product_id}`} className="product-container-link">
            <div className="product-container">
                <div className="product-image">
                    <img src={product.product_avatar} alt={product.product_avatar} />
                </div>
                <div className="product-name">
                    {SlugStrTitle(product.product_title, 30)}
                </div>

                <div className="product-rating-medium">
                    {product.product_rating ?
                        <>
                            <ReactStars
                                half={true}
                                edit={false}
                                value={product.product_rating ? Number.parseFloat(product.product_rating.toFixed(1)) : 0}
                                count={5}
                                size={22}
                                color2={'#ffd700'}
                            />
                            <div className="product-rating-txt">
                                {product.product_rating ? product.product_rating.toFixed(1) : 0}
                            </div>
                        </>

                        :
                        <div className="product-rating-none">
                            Chưa đánh giá
                        </div>
                    }
                </div>

                {product && product.product_sale! !== 0 &&
                    <div className="price-sale">
                        {vnd(product.product_price!)}đ
                    </div>
                }

                <div className="price">
                    {product && vnd(product.product_price * (100 - product.product_sale) / 100)}đ
                </div>
            </div>
        </NavLink>
    )
}
