import { Link } from 'react-router-dom';
import { CategoryModel } from '../../redux';
import './category.css'

export default function Category(props:any) {
    const {activeIndex,index,onTab,type} = props;
    const { category }:{ category:CategoryModel} = props;
    switch (type) {
        case 'sub':  
            if (window.innerWidth < 779) {
                return(
                    <Link className="link-sub-cat" to={`/products/categories/${category.category_id}`}>
                        <div className='category-sub' >
                            <div className="img-category-sub">
                                <img src={category.category_image} alt="" />
                            </div>
                            <div className= "category-title-sub">{category.category_name}</div>
                        </div>
                    </Link> 
                )  
            }  
            else{
                return(
                    <div className='category-sub' >
                        <div className="img-category-sub">
                            <img src={category.category_image} alt="" />
                        </div>
                        <div onClick={onTab} className= {activeIndex === index ?"category-title-sub category-title-sub-avtive":"category-title-sub" }>{category.category_name}</div>
                    </div>
                )
            }
        case 'char':    
            return (
            <div onClick={onTab} className={activeIndex === index ? "category-container-char category-container-active" : "category-container-char"} >
                <div className="category-title">{category.category_name}</div>
            </div>
        )
        default:
            return (
            <div onClick={onTab} className={activeIndex === index ? "category-container category-container-active" : "category-container"} >
                <div className="img-category">
                    <img src={category.category_image} alt="" />
                </div>
                <div className="category-title">{category.category_name}</div>
            </div>
        )
    
    }
    
}
