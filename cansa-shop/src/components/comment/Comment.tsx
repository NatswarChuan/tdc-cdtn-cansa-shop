import ReactStars from 'react-stars'
import './comment.css'
import { CommentModel } from './../../redux';

export default function Comment(props:any) {
    const comment:CommentModel = props.comment;
    return (
        <div className="comment-container">
           
            <div className="comment-avatar-name">
                <img src={comment.user?.user_avatar} alt={comment.user?.user_avatar} />
                <div className="comment-name">
                    {comment.user?.user_name}
                    <div className="comment-rating-medium">
                        <ReactStars
                            half ={true}
                            edit={false}
                            value={comment.comment_rating}
                            count={5}
                            size={24}
                            color2={'#ffd700'} 
                        />
                        <div className="comment-rating-txt">
                            ({comment.comment_rating})
                        </div>
                    </div>
                </div>
            </div>

            <div className="comment-cmt">
              {comment.comment_content}
            </div>
            
        </div>
    )
}
