import { Add, Delete, Remove } from '@material-ui/icons'
import { Link } from 'react-router-dom';
import { TextToSlug, vnd } from '../../consts/Selector';
import { CartItemModel } from '../../redux';
import './cartcard.css'

export default function CartCard(props: any) {
    const quantity= 1
    const { cart,onTap,isLoading } : { cart: CartItemModel,onTap: Function, isLoading:Boolean}  = props;
   
    const onPlus = () => {
        const _qty:number = Number(cart.qty); 
        const qty: number = _qty + 1;
        if (cart.product.product_quantity && quantity < cart.product.product_quantity) {
            onTap(cart.product.product_id, qty);
        } else if (!cart.product.product_quantity) {
            onTap(cart.product.product_id, qty);
        }
    }

    const onMinus = () => {
        if (cart.qty > 1) {
            const qty: number = cart.qty - 1;
            onTap(cart.product.product_id, qty);
        }
    }

    const onDelete = () => {
        onTap(cart.product.product_id, 0);
        
    }

    return (
        <div className="cartcard-conainer">
            <div className="cartcard-conainer-con">
                <Link to={`/product-detail/${TextToSlug(cart.product.product_title)}/${cart.product.product_id}`}>
                <div className="cartcard-img">
                    <img src={cart.product.product_avatar} alt="" />
                </div>
               </Link>
                <div className="cartcard-left-conatainer">
                    <div className="cartcard-item">
                        <div className="cartcard-detail">
                            {cart.product.product_title}
                        </div>
                        <div className="cartcard-delete">
                            <Delete style={{ color: '#f85d6c' }} onClick={onDelete} />
                        </div>
                    </div>

                    <div className="cartcard-item">
                        <div className="cartcard-price-sale">
                            {cart.product.product_sale ? `${vnd(Number(cart.product.product_price))}đ `: ''}
                        </div>
                        <div className="cartcard-price">
                            {vnd(Number(cart.product.product_price * (100 - cart.product.product_sale) / 100))}đ
                        </div>
                    </div>

                    <div className="cartcard-item">
                        <div className="cartcard-quantity">
                            {
                                !isLoading ? 
                                <>
                                    <div className="cartcard-btn"><Remove /></div>
                                    <div className="cartcard-quantity-number">{cart.qty}</div>
                                    <div className="cartcard-btn"><Add /></div>
                                </>
                                :
                                <>
                                    <button onClick={onMinus} className="cartcard-btn"><Remove /></button>
                                    <div className="cartcard-quantity-number">{cart.qty}</div>
                                    <button onClick={onPlus} className="cartcard-btn"><Add /></button>
                                </>
                            }
                        </div>
                        <div className="cartcard-price-total">
                            {vnd(Number((cart.product.product_price * (100 - cart.product.product_sale) / 100) * cart.qty))}đ
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
