import './footer.css';
import { Facebook, Instagram, LinkedIn, MailOutline, Phone, Room } from '@material-ui/icons';

export default function Footer() {
    return (
        <div className="footer-container">
            <div className="footer-container-con">
                <div className="footer-left">
                    <h2 className="footer__title">CANSA</h2>
                    <span className="footer-span">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, recusandae laborum provident cum laudantium placeat a optio eum nisi nesciunt veritatis fuga! Explicabo cum adipisci quaerat id iste iure doloremque?</span>
                </div>
                <div className="footer-center">
                    <h2 className="footer__title">THEO DÕI CHÚNG TÔI</h2>
                    <ul className="footer-list">
                        <li className="footer-item footer-link"><a className="contact-link" href="https://www.facebook.com/hoanganhh2306" target="_blank" rel="noreferrer"><Facebook /><div className="contact-link-title">Facebook</div></a></li>
                        <li className="footer-item footer-link"><a className="contact-link" href="https://www.instagram.com/hoanganhguitar/" target="_blank" rel="noreferrer"><Instagram /><div className="contact-link-title">Instagram</div></a> </li>
                        <li className="footer-item footer-link"><a className="contact-link" href="http://103.207.38.200:3002/api/image/photo/222.png/e4611a028c71342a5b083d2cbf59c494" target="_blank" rel="noreferrer"><LinkedIn /><div className="contact-link-title">LinkedIn</div></a></li>
                    </ul>
                </div>
                <div className="footer-right">
                    <h2 className="footer__title">CONTACT</h2>
                    <ul className="footer-list">
                        <li className="footer-item"><Room /> <div className="contact-title">69 Thành phố Hồ Chí Minh</div></li>
                        <li className="footer-item"><Phone /> <div className="contact-title">+123 456 789</div></li>
                        <li className="footer-item"><MailOutline /> <div className="contact-title">hoanganh34k@gmail.com</div></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
