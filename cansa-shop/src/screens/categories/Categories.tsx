import { ArrowBackIosOutlined, ArrowForwardIosOutlined } from '@material-ui/icons'
import { useEffect, useRef, useState } from 'react'
import Slider from 'react-slick'
import Category from '../../components/category/Category'
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import Product from '../../components/Product/Product'
import './categories.css'
import { CategoryModel, CategoryState, getCategory, getProductsCategory, ProductDataPage, ProductModel, ProductState, State } from '../../redux'
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../../components/loading/Loading'

export default function Categories() {
    const categoriesSlider = useRef<any>();
    const [productList, setProductList] = useState<ProductModel[]>([] as ProductModel[]);
    const [catSub, setCatSub] = useState<CategoryModel[]>([] as CategoryModel[]);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingProduct, setIsLoadingProduct] = useState(false);
    const [catergoryIndex, setCategoryIndex] = useState(0);
    const [catergorySubIndex, setCategorySubIndex] = useState(0);
    const categoriesState: CategoryState = useSelector((state: State) => state.categoryReducer);
    const { categories }: { categories: CategoryModel[] } = categoriesState;
    const productState: ProductState = useSelector((state: State) => state.productReducer);
    const { productCategory }: { productCategory:ProductDataPage } = productState;
    const [page, setPage] = useState<number>(1);
    const [nextPage, setNextPage] = useState(false)
    const [sort, setSort] = useState<string>('');
    const [begin, setBegin] = useState<string>('');
    const [end, setEnd] = useState<string>('');
    const [idCatSub, setidCatSub] = useState(0);
    const [isLoadMore, setIsLoadMore] = useState(false)
    
    const dispatch = useDispatch();

    function next(refSlider: any) {
        refSlider.current.slickNext();
    };
    function previous(refSlider: any) {
        refSlider.current.slickPrev();
    };

    useEffect(() => {
        if (catSub.length) {
            dispatch(getProductsCategory(catSub[0].category_id));
            setidCatSub(catSub[0].category_id)
        }
    }, [catSub])

    useEffect(() => {
        setIsLoading(true)
        categories.length !== 0 &&
        setCatSub(categories[catergoryIndex].categories);
        
    }, [categories])

    useEffect(() => {
        setIsLoadMore(false)  
        setIsLoadingProduct(false)
        setNextPage(productCategory.isload)
        setProductList(productCategory.data)
    }, [productState])


    useEffect(() => {
        window.scrollTo(0, 0)
        setIsLoading(false)
        dispatch(getCategory());
    }, [])

    const onTapCatSub = (index: number) => {
        if (categories[index].categories.length) {
            setCategorySubIndex(0)
            setCategoryIndex(index)
            setCatSub(categories[index].categories);
        }else{
            alert('Danh muc không khả thi')
        }
        
    }

    const onTapCatSubPro = (index: number, id: number) => {
        setIsLoadingProduct(true)
        setCategorySubIndex(index)
        dispatch(getProductsCategory(id));
        setidCatSub(id);
    }

    useEffect(() => {
        dispatch(getProductsCategory(idCatSub,"","","",page));
    }, [page])

    const getNewPage = () =>{
        setIsLoadMore(true)  
        setPage(page + 1);  
    }

    const filterPrice = (data: string) => {
        switch (data) {
            case '1':
                dispatch(getProductsCategory(catSub[0].category_id,sort,'0','1000000'))
                setBegin('0');
                setEnd('1000000')
                break;
            case '2': 
                dispatch(getProductsCategory(catSub[0].category_id,sort,'1000000','10000000'))
                setBegin('1000000');
                setEnd('10000000')
                break;
            case '3':
                dispatch(getProductsCategory(catSub[0].category_id,sort,'10000000','100000000'))
                setBegin('1000000');
                setEnd('100000000');
                break;
            default:
                dispatch(getProductsCategory(catSub[0].category_id,sort,'',''))
                setBegin('');
                setEnd('');
                break;
        }
    }
    const sortName = (data:string)=>{
        switch (data) {
            case '1':
                dispatch(getProductsCategory(catSub[0].category_id,'asc',begin,end));
                setSort('desc')
                break;
            case '2':
                dispatch(getProductsCategory(catSub[0].category_id,'desc',begin,end));
                setSort('asc')
                break;
            default:
                dispatch(getProductsCategory(catSub[0].category_id,'',begin,end));
                setSort('')
                break;
        }
        
    }

    return (
        <>
            <HeaderTop />
            {
                categories && isLoading ?
                    <div className="categories-container">
                        <div className="categories-container-con">
                            <button className="btn-cat btn-prev-cat" onClick={() => previous(categoriesSlider)}><ArrowBackIosOutlined style={{ color: "black", fontSize: 21 }} /></button>
                            <Slider ref={categoriesSlider}  {...settings}>
                                {
                                    categories ?
                                        categories.map((item: CategoryModel, index: number) => <Category category={item} type='image' onTab={() => onTapCatSub(index)} index={index} activeIndex={catergoryIndex} key={index} />)
                                        : <Loading />
                                }
                            </Slider>
                            <button className="btn-cat btn-next-cat" onClick={() => next(categoriesSlider)}><ArrowForwardIosOutlined style={{ color: "black", fontSize: 20 }} /></button>
                        </div>


                        <div className="categories-container-con2">

                            <div className="categories-left">
                                <div className="categories-left-title">
                                    DANH MỤC SẢN PHẨM
                                </div>
                                {
                                    catSub &&
                                    catSub.map((item: CategoryModel, index: number) => <Category category={item} key={index} onTab={() => onTapCatSubPro(index, item.category_id)} index={index} activeIndex={catergorySubIndex} type="sub" />)
                                }
                            </div>

                            <div className="categories-product-lt">
                                <div className="categories-filter">
                                    <div className="search-container-sort">
                                        <div>Filter : </div>
                                        <select className="select-sort" defaultValue={0} onChange={(e: any) => filterPrice(e.currentTarget.value)}>
                                            <option value={0}>All</option>
                                            <option value={1}> &lt; 1.000.000đ </option>
                                            <option value={2}> 1.000.000đ - 10.000.000đ </option>
                                            <option value={3}> &gt; 10.000.000đ </option>
                                        </select>
                                    </div>
                                    <div className="search-container-filter">
                                        <div>Sort : </div>
                                        <select className="select-sort" defaultValue={0} onChange={(e: any) => sortName(e.currentTarget.value)}>
                                            <option value={0}>All</option>
                                            <option value={1}>A - Z</option>
                                            <option value={2}>Z - A</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="categories-right-product">
                                    {
                                            isLoadingProduct ?
                                            <Loading type="screen" />
                                            :
                                            productList ?
                                            productList.map((item: ProductModel, index: number) => <Product product={item} key={index} />)
                                            :
                                            <div className="categories-product-null">Không có sản phẩm . . . </div>
                                    }
                                </div>
                                {
                                    !isLoadMore ?
                                    (productList && nextPage) &&
                                    <div onClick={getNewPage} className="categories-load-new">
                                        Xem thêm . . .  
                                    </div>
                                    :
                                    <div className="categories-load-more">
                                        <Loading type="max" />   
                                    </div>
                                }
                                   
                                
                            </div>

                        </div>
                    </div>
                    :
                    <div className="productDetail-load">
                        <Loading type="screen"/>
                    </div>
            }
            <Footer />
        </>
    )
}
const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 8,
    slidesToScroll: 4,
    initialSlide: 0,
    arrows: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                initialSlide: 0
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }
    ]
};