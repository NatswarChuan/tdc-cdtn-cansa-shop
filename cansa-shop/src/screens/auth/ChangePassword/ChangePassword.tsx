import React, { useState } from 'react';
import axios from "axios";
import Alert from '../../../components/Alert'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import './style3.css'
import HeaderTop from '../../../components/header/HeaderTop';

function ChangePassword() {
    const [message, setMessage] = useState('')
    const changepasswordUser = async (e: any) => {
        e.preventDefault()
        const payload = new FormData(e.target);
        axios.post('http://happy_eyes.test/api/user/login', payload)
            .then(function (response: any) {
                setMessage(`Register user success`)
            })
            .catch(function (error: any) {
                const err = error.response.data.message
                setMessage(err)
            });
    }
    // Regex form

    const [isError, setIsError] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");


    function checkValuePass(e: any) {
        setConfirmPassword(e.target.value);
        if (e.target.value === password) {
            setIsError("Mật khẩu gồm 6 ký tự trở lên gồm chữ và số ! ");
        } else {
            setIsError("");
        }
    }

    function checkValuCorfirm(e: any) {
        setPassword(e.target.value);
        if (e.target.value !== confirmPassword) {
            setIsError("Mật khẩu không trùng khớp! Vui lòng nhập lại...");
        } else {
            setIsError("");
        }
    }

    return (
        <>
            <HeaderTop></HeaderTop>
            <section className="change_relative">
                <Alert message={message} />
                <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hiden">
                    <form id="change_form" onSubmit={(e) => changepasswordUser(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                        <h1 className="change-container text-2xl font-bold w-screen">Thay Đổi Mật Khẩu</h1>
                        <p className="change_text text-gray-400 text-xl mt-5">
                            Thay đổi thông tin mật khẩu của bạn
                        </p>
                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Mật khẩu hiện tại</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="currentpassword"
                                type="password"
                                placeholder="Nhập mật khẩu" />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Mật khẩu mới</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuePass(e)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Nhập lại mật khẩu mới</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuCorfirm(e)} />
                        </label>

                        <button type="submit" className="mt-6 btn font-bold w-full">Xác nhận</button>
                        <p className="isError">{isError}</p>
                        <div className="h-px bg-gray-200 mt-8 relative">
                            <span className="absolute absolute-x absolute-y bg-white px-3 mt-px text-sm text-gray-400"></span>
                        </div>
                        <p className="text-gray-400 text-center mt-5">
                        Bạn đã có tài khoản?
                            <a className="text-indigo-700 underline ml-1">Đăng nhập</a>
                        </p>
                    </form>
                    <section className="img_logo hidden md:block">
                        <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                    </section>
                </div>
            </section>
        </>
    );
}

export default ChangePassword;
