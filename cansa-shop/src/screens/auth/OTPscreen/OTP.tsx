import React, { useState, useEffect, useRef } from 'react';
import Alert from '../../../components/Alert'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import './style6.css'
import HeaderTop from '../../../components/header/HeaderTop';
import { useParams } from 'react-router';
import { useHistory } from "react-router-dom";
import { ForgottPasswordOTP,ForgottPassword } from '../../../redux/actions/userActions'
import { State, UserStage } from '../../../redux';
import { useDispatch, useSelector } from 'react-redux';

function LoginUser() {
    let { email } = useParams<any>();
    const [message, setMessage] = useState('')
    const history = useHistory();
    const dispatch = useDispatch();
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, status }: { check: boolean, status: string } = userState;

    // Nhập mã OTP
    const [pinText1, setpinText1] = useState<string>('');
    const [pinText2, setpinText2] = useState<string>('');
    const [pinText3, setpinText3] = useState<string>('');
    const [pinText4, setpinText4] = useState<string>('');
    const [pinText5, setpinText5] = useState<string>('');
    const [pinText6, setpinText6] = useState<string>('');
    const pinInputRef1 = useRef<any>();
    const pinInputRef2 = useRef<any>();
    const pinInputRef3 = useRef<any>();
    const pinInputRef4 = useRef<any>();
    const pinInputRef5 = useRef<any>();
    const pinInputRef6 = useRef<any>();


    const updatePinText1 = (pinText1: string) => {
        setpinText1(pinText1);
        if (pinText1 !== "") {
            pinInputRef2.current?.focus()
        }
    };
    const updatePinText2 = (pinText2: string) => {
        setpinText2(pinText2);
        if (pinText2 !== "") {
            pinInputRef3.current?.focus()
        }else if(pinText2 == ""){
            pinInputRef1.current?.focus()
        }
    };

    const updatePinText3 = (pinText3: string) => {
        setpinText3(pinText3);
        if (pinText3 !== "") {
            pinInputRef4.current?.focus()
        }else if(pinText3 == ""){
            pinInputRef2.current?.focus()
        }
    };

    const updatePinText4 = (pinText4: string) => {
        setpinText4(pinText4);
        if (pinText4 !== "") {
            pinInputRef5.current?.focus()
        }else if(pinText4 == ""){
            pinInputRef3.current?.focus()
        }
    };

    const updatePinText5 = (pinText5: string) => {
        setpinText5(pinText5);
        if (pinText5 !== "") {
            pinInputRef6.current?.focus()
        }else if(pinText5 == ""){
            pinInputRef4.current?.focus()
        }
    };

    const updatePinText6 = (pinText6: string) => {
        setpinText6(pinText6);
        if (pinText6 == "") {
            pinInputRef5.current?.focus()
        }
    };
    // Time out
    const [time, setTime] = useState(60);
    useEffect(() => {
        if (time > 0) {
            setTimeout(() => {
                setTime(time - 1)
            }, 1000);
        } else {
            // Alert.alert('Thông báo',"Vui lòng resend mã code!!")
        }
    }, [time]);
    useEffect(() => {
        setTime(time)
    }, []);

    useEffect(()=>{
        if(check === true){
            let codePin = `${pinText1}${pinText2}${pinText3}${pinText4}${pinText5}${pinText6}`
            history.push(`/PasswordRetrieval/${email}/${codePin}`);
        }
    },[check])
    const emailOTP = async (e: any) => {
        e.preventDefault()
        const payload = new FormData(e.target);
        //  ForgottPasswordOTP
        let codePin = `${pinText1}${pinText2}${pinText3}${pinText4}${pinText5}${pinText6}`
        dispatch(ForgottPasswordOTP(email,codePin))
    }
    const reSend = ()=>{
        dispatch(ForgottPassword(email))
    }
    useEffect(() => {
        if (status === 'success') {
            setTime(60)
        }
    }, [status])
    return (
        <>
        <HeaderTop></HeaderTop>
        <section className="otp_relative">
            <Alert message={message} />
            <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hidden">
                <form id="otp_form" onSubmit={(e) => emailOTP(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                    <h1 className="title-verfication text-2xl font-bold w-screen">Verfication OTP Code</h1>
                    <p className="input-otp text-gray-400 text-xl mt-5">
                    Nhập mã OTP của bạn được gửi qua Email
                    </p>
                    <label className="otp-code block text-grey-darker text-sm mb-1 mt-4" >
                        <input
                            name="otp1"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText1(e.target.value)}
                            value={pinText1}
                            ref={pinInputRef1}
                            maxLength={1}
                        />
                        <input
                            name="otp2"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText2(e.target.value)}
                            value={pinText2}
                            ref={pinInputRef2}
                            maxLength={1}
                        />
                        <input
                            name="otp3"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText3(e.target.value)}
                            value={pinText3}
                            ref={pinInputRef3}
                            maxLength={1}
                        />
                        <input
                            name="otp4"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText4(e.target.value)}
                            value={pinText4}
                            ref={pinInputRef4}
                            maxLength={1}
                        />
                        <input
                            name="otp5"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText5(e.target.value)}
                            value={pinText5}
                            ref={pinInputRef5}
                            maxLength={1}
                        />
                        <input
                            name="otp6"
                            type="text"
                            autoComplete="off"
                            className="otpInput"
                            onChange={e =>updatePinText6(e.target.value)}
                            value={pinText6}
                            ref={pinInputRef6}
                            maxLength={1}
                        />
                    </label>

                   <button type="submit" className="mt-6 btn font-bold w-full">Xác nhận</button>

                    <p className="text-gray-400 text-center mt-5">
                        Resend OTP in: {time} second
                    </p>

                    <p  className="text-gray-400 text-center mt-5">
                        <a onClick={()=>{reSend()}}  className="btnResend text-indigo-700 underline ml-1">Resend OTP</a>
                    </p>
                </form>

                <section className="img_logo hidden md:block">
                    <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                </section>
            </div>
        </section>
        </>

    );
}
export default LoginUser;