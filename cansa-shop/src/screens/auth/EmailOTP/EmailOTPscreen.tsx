import React, { useEffect, useState } from 'react';
import Alert from '../../../components/Alert'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import './style5.css'
import HeaderTop from '../../../components/header/HeaderTop';
import { useHistory } from "react-router-dom";
import {  ForgottPassword } from '../../../redux/actions/userActions'
import { State, UserStage } from '../../../redux';
import { useDispatch, useSelector } from 'react-redux';

function LoginUser() {
    const [message, setMessage] = useState('')
    const [email, setEmail] = useState("");
    const history = useHistory();
    const dispatch = useDispatch();
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, status }: { check: boolean, status: string } = userState;

    useEffect(() => {
        if (status === 'success') {
            history.push(`/OTP/${email}`);
        }
    }, [status])

    const emailOTP = async (e: any) => {
        e.preventDefault()
        const payload = new FormData(e.target);
        dispatch(ForgottPassword(email))

    }
    return (
        <>
            <HeaderTop></HeaderTop>
            <section className="emailotp_relative">
                <Alert message={message} />
                <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hidden">
                    <form id="emailotp_form" onSubmit={(e) => emailOTP(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                        <h1 className="email-otp text-2xl font-bold w-screen">Email OTP</h1>
                        <p className="emailotp_text text-gray-400 text-xl mt-5">
                            Chúng tôi sẽ gửi mã 6 chữ số đến email của bạn để xác minh
                        </p>

                        <label className="block text-grey-darker text-sm mb-1 mt-4" >
                            <span className="block mb-1">Email Address</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                type="email"
                                name="email"
                                placeholder="you@example.com"
                                onChange={(e: any) => setEmail(e.target.value)}
                            />
                        </label>

                        <button type="submit" className="mt-6 btn font-bold w-full">Xác nhận</button>
                    </form>

                    <section className="img_logo hidden md:block">
                        <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                    </section>
                </div>
            </section>
        </>
    );
}
export default LoginUser;