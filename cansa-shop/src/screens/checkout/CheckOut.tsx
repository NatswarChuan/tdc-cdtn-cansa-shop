import React, { useEffect, useState } from 'react'
import { Col, Form, Row } from 'react-bootstrap'
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import PaymentIcon from '@material-ui/icons/Payment';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios';
import './checkout.css'
import { addOder, CartModel, getCart, getUserInfo, State, UserModel } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';
import { cansa, vnd } from '../../consts/Selector';
import { useHistory } from 'react-router';
import IsLogin from '../../components/IsLogin';

export default function CheckOut() {
    const dispatch = useDispatch();
    const { cart }: { cart: CartModel } = useSelector((state: State) => state.cartReducer);
    const { userInfor }: { userInfor: UserModel } = useSelector((state: State) => state.userReducer);
    const oderState = useSelector((state: State) => state.oderReducer);
    const { status } = oderState;
    const history = useHistory();

    useEffect(() => {
        dispatch(getCart());
        dispatch(getUserInfo());
    }, [])

    let temp = [
        {
            value: {
                code: 0,
                name: " "
            },
            label: " "
        }
    ]

    const [thanhPho, setThanhPho] = useState(temp);
    const [quanHuyen, setQuanHuyen] = useState(temp);
    const [phuongXa, setPhuongXa] = useState(temp);


    const [_thanhPho, _setThanhPho] = useState('');
    const [_quanHuyen, _setQuanHuyen] = useState('');
    const [_phuongXa, _setPhuongXa] = useState('');
    const [_soNha, _setSoNha] = useState('');
    const [_sdt, _setsdt] = useState('');

    useEffect(() => {
        if(userInfor){
            _setsdt(userInfor.user_phone);
        }
    },[userInfor])

    useEffect(() => {
        (async function () {
            await axios.get(`${cansa[1]}/api/address/city/e4611a028c71342a5b083d2cbf59c494`)
                .then((response: any) => {
                    let { results } = response.data;
                    let tempArr = results.map((item: any) => {
                        return { label: item.name, value: item };
                    });
                    setThanhPho(tempArr);

                })
        })();
    }, [])

    const getDistrict = (code: string) => {
        const data = code.split(':');
        _setThanhPho(data[1]);
        (async function () {
            await axios.get(`${cansa[1]}/api/address/district/${data[0]}/e4611a028c71342a5b083d2cbf59c494`)
                .then((response: any) => {
                    let { results } = response.data;
                    let tempArr = results.map((item: any) => {
                        return { label: item.name, value: item };
                    });
                    setQuanHuyen(tempArr);
                })
        })();
    };

    const getWard = (code: string) => {
        const data = code.split(':');
        _setQuanHuyen(data[1]);
        (async function () {
            await axios.get(`${cansa[1]}/api/address/ward/${data[0]}/e4611a028c71342a5b083d2cbf59c494`)
                .then((response: any) => {
                    let { results } = response.data;
                    let tempArr = results.map((item: any) => {
                        return { label: item.name, value: item };
                    });
                    setPhuongXa(tempArr);
                })
        })();
    };

    const oder = () => {
        if (userInfor) {
            if (_quanHuyen && _thanhPho && _soNha && /(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(_sdt)) {
                let diaChi = `${_soNha},${_phuongXa && _phuongXa + ','}${_quanHuyen},${_thanhPho}`;
                let user_id = userInfor.user_id;
                dispatch(addOder(diaChi, _sdt, user_id));
            }
            else {
                alert("Điền sai hoặc thiếu thông tin");
            }
        }else {
            alert("Chưa đăng nhập");
        }
    }

    useEffect(() => {
        if(status){
            alert("Đặt hàng thành công");
            history.push("/");
        }
    },[oderState]);

    return (
        <>
            <IsLogin />
            <HeaderTop></HeaderTop>

            <div className="checkout_container">
                <div className="container_con_checkout">
                    <Row className="checkout_row">
                        <Col className="col_infor" md={7}>
                            <div className="title_checkout">
                                <h3>Thông Tin Thanh Toán</h3>
                            </div>

                            <Form>
                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Họ tên</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <Form.Control className="infor_checkout" plaintext readOnly defaultValue={userInfor.user_real_name} />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="infor">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Email</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <Form.Control className="infor_checkout" plaintext readOnly defaultValue={userInfor.user_email} />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Số điện thoại</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <Form.Control className="infor_checkout" defaultValue={userInfor.user_phone} onChange={(e:any)=>_setsdt(e.currentTarget.value)}/>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Địa chỉ thanh toán</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <Form.Control className="infor_checkout" defaultValue="" onChange={(e:any)=>_setSoNha(e.currentTarget.value)}/>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Thành phố</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <select
                                            className="select-sort"
                                            onChange={(e: any) => getDistrict(e.currentTarget.value)}>
                                            <option value={0}>Thành phố</option>
                                            {
                                                thanhPho && thanhPho.map((item,index) =>
                                                    <option key={index} value={`${item.value.code}:${item.value.name}`}>{item.label}</option>
                                                )
                                            }
                                        </select>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Quận/Huyện</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <select
                                            className="select-sort"
                                            onChange={(e: any) => getWard(e.currentTarget.value)}>
                                            <option value={0}>Quận/Huyện</option>
                                            {
                                                quanHuyen && quanHuyen.map((item,index) =>
                                                    <option key={index} value={`${item.value.code}:${item.value.name}`}>{item.label}</option>
                                                )
                                            }
                                        </select>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="infor_checkout">
                                    <Form.Label column md="2" className="lb_checkout">
                                        <h5>Phường/Xã</h5>
                                    </Form.Label>
                                    <Col md="10">
                                        <select
                                            className="select-sort"
                                            onChange={(e: any) => _setPhuongXa(e.currentTarget.value)}>
                                            <option value={0}>Phường/Xã</option>
                                            {
                                                phuongXa && phuongXa.map((item,index) =>
                                                    <option key={index} value={`${item.value.name}`}>{item.label}</option>
                                                )
                                            }
                                        </select>
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Col>


                        <Col className="col_check" md={4}>
                            <div className="title_payment">
                                <h5>Phương thức thanh toán</h5>
                            </div>

                            <div className="border_payment">
                                <div className="icon_title_paymet">
                                    <PaymentIcon className="icon_payment"></PaymentIcon>
                                    Thanh toán khi nhận hàng
                                </div>
                                <span className="icon_checkbox">
                                    <CheckCircleIcon className="icon_check"></CheckCircleIcon>
                                </span>
                                <div className="title_mini_method">
                                    Thanh toán khi nhận hàng
                                </div>
                            </div>

                            <div className="title_order">
                                <h5>Thông tin đơn hàng</h5>
                            </div>

                            <Row>
                                <Col md={6}>
                                    <div className="title_price_product" id="title_ic">Tổng giá đơn hàng</div>
                                </Col>
                                <Col md={6}>
                                    <div className="price_all_product" id="price_id">{vnd(Number(cart.sub_price))} đ</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <div className="title_price_ship" id="title_ic">Phí giao hàng</div>
                                </Col>
                                <Col md={6}>
                                    <div className="price_ship" id="price_id">{vnd(Number(cart.ship))} đ</div>
                                </Col>
                            </Row>
                            <hr />
                            <Row>
                                <Col md={6}>
                                    <div className="title_price_all">Tổng cộng</div>
                                </Col>
                                <Col md={6}>
                                    <div className="price_all" id="price_id">{vnd(Number(cart.total_price))} đ</div>
                                </Col>
                            </Row>

                            <button onClick={oder} className="checkout_btn_order">ĐẶT HÀNG</button>
                        </Col>
                    </Row>

                </div>
            </div>

            <Footer></Footer>
        </>
    )
}
