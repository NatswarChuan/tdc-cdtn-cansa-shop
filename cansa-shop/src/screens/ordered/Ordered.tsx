import React, { useEffect } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import './ordered.css'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { getAllOder, OderItemModel, OderModel, OderState, State, updateOder, UserModel, UserStage } from '../../redux'
import { getUserInfo } from '../../redux/actions/userActions'
import { vnd } from '../../consts/Selector'
import moment from 'moment'
import IsLogin from '../../components/IsLogin'

export default function Ordered() {
    const [openDelete, setOpenDelete] = React.useState(false)
    const orderState: OderState = useSelector((state: State) => state.oderReducer);
    const { oderList }: { oderList: OderModel[] } = orderState;
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { userInfor }: { userInfor: UserModel } = userState;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserInfo());
    }, []);

    useEffect(() => {
        if (userInfor) {
            dispatch(getAllOder(userInfor.user_id));
        }
    }, [userInfor])

    const handleClickOpenDelete = () => {
        setOpenDelete(true);
    }

    function handleCloseDelete(oder_id: number = -1) {
        if (oder_id !== -1) {
            dispatch(updateOder(userInfor.user_id, oder_id))
        }
        setOpenDelete(false)
    }


    return (
        <>
            <IsLogin />
            <HeaderTop></HeaderTop>
            <div className="ordered_container">
                <div className="container_con_ordered">
                    <div className="title_ordered">
                        Danh sách đơn hàng
                    </div>


                    {/* Hiển thị list sản phẩm */}

                    {
                        oderList && oderList.map((oderItem: OderModel, index: number) => {
                            return (
                                <div className="list_ordered" key={index}>
                                    <div className="name_shop">
                                        <span className="ordered_time">{moment(oderItem.oder_date).format('DD/MM/YYYY')}</span>
                                        {
                                            (oderItem.status === 1) ?
                                                <button className="ordered_delete" onClick={handleClickOpenDelete}>
                                                    Huỷ đơn hàng
                                                </button> :
                                                (oderItem.status === 2 || oderItem.status === 3) ?
                                                    <div className="ordered_status_accept">Đang sử lý</div> :
                                                    (oderItem.status === 4) ?
                                                        <div className="ordered_status_accept">Đã nhận</div> :
                                                        (oderItem.status === 0) &&
                                                        <div className="ordered_status_cancel">Đã hủy</div>
                                        }

                                        <Dialog
                                            open={openDelete}
                                            onClose={() => handleCloseDelete()}
                                            aria-labelledby="draggable-dialog-title"
                                        >
                                            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                                                Lưu ý
                                            </DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    Bạn có chắc chắn muốn huỷ đơn hàng này không?
                                                </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button id="ordered_btn_in_update" autoFocus onClick={() => handleCloseDelete()} >
                                                    Hủy
                                                </Button>
                                                <Button id="ordered_btn_in_update_delete" onClick={() => handleCloseDelete(Number(oderItem.oder_id))} >
                                                    Xác nhận
                                                </Button>
                                            </DialogActions>
                                        </Dialog>

                                        <button className="ordered_btn_detail">
                                            <Link className="link_btn_ordered" to={`/orderdetail/${oderItem.oder_id}`}>Chi tiết</Link>
                                        </button>
                                    </div>
                                    <div className="title_row_ordered">
                                        {
                                            oderItem.product_oder && oderItem.product_oder.map((product: OderItemModel, index: number) => {
                                                return (
                                                    <Row className="list_product" key={index}>
                                                        <Col md={4}>
                                                            <div className="order_img_product">
                                                                <img src={product.product.product_avatar} />
                                                            </div>
                                                        </Col>
                                                        <Col md={4} id="mobile_responsive">
                                                            <div className="order_price_product">
                                                                {vnd((product.product.product_price * (100 - product.product.product_sale) / 100) * product.product_quantity)}đ
                                                            </div>
                                                        </Col>
                                                        <Col md={4} id="mobile_responsive">
                                                            Qty: {product.product_quantity}
                                                        </Col>
                                                    </Row>
                                                )
                                            })
                                        }

                                    </div>
                                </div>
                            )
                        })

                    }

                </div>
            </div>
            <Footer></Footer>
        </>
    )
}
