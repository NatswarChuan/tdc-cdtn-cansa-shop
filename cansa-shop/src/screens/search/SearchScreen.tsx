import './search.css'
import HeaderTop from './../../components/header/HeaderTop';
import Footer from '../../components/footer/Footer';
import Product from '../../components/Product/Product';
import { useEffect, useState } from 'react';
import { getProductsCategory, getProductsSearch, ProductModel, ProductState, State, ProductDataPage } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import Loading from '../../components/loading/Loading';

export default function SearchScreen() {
    let { title,data } = useParams<any>();
    const [products, setProducts] = useState<any>([] as ProductModel[]);
    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch();
    const productState:ProductState = useSelector((state: State) => state.productReducer);
    const { productSearch, productCategory } : { productSearch:ProductDataPage , productCategory:ProductDataPage} = productState;
    const [nextPage, setNextPage] = useState(false)
    const [page, setPage] = useState<number>(1);
    const [sort, setSort] = useState<string>('');
    const [begin, setBegin] = useState<string>('');
    const [end, setEnd] = useState<string>('');
    const [isLoadMore, setisLoadMore] = useState(false)

    useEffect(() => {
        setisLoadMore(true);
        switch (title) {
            case 'search':
                dispatch(getProductsSearch(data,sort,begin,end,page));
                break;
            default:
                dispatch(getProductsCategory(data,sort,begin,end, page));
                break;
        }
    }, [page])

    const filterPrice = (key: number) => {
        switch (key) {
            case 1:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,sort,'0','1000000',page))
                    setBegin('0');
                    setEnd('1000000')
                }
                else{
                    dispatch(getProductsCategory(data,sort,'0','1000000',page))
                    setBegin('0');
                    setEnd('1000000')
                }
                break;
            case 2: 
                if (title === 'search') {
                    dispatch(getProductsSearch(data,sort,'1000000','10000000',page))
                    setBegin('1000000');
                    setEnd('10000000')
                }
                else{
                    dispatch(getProductsCategory(data,sort,'1000000','10000000',page))
                    setBegin('1000000');
                    setEnd('10000000')
                }
                break;
            case 3:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,sort,'10000000','100000000',page))
                    setBegin('10000000');
                    setEnd('100000000')
                }
                else{
                    dispatch(getProductsCategory(data,sort,'10000000','100000000',page))
                    setBegin('10000000');
                    setEnd('100000000')
                }
                break;
            default:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,sort,'','',page))
                    setBegin('');
                    setEnd('')
                }
                else{
                    dispatch(getProductsCategory(data,sort,'','',page))
                    setBegin('');
                    setEnd('')
                }
                break;
        }
        
    }
    const sortName = (key:number)=>{
        switch (key) {
            case 1:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,'asc',begin,end,page))
                    setSort('asc');
                }
                else{
                    dispatch(getProductsCategory(data,'asc',begin,end,page))
                    setSort('asc');
                }
                break;
            case 2:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,'desc',begin,end,page))
                    setSort('desc');
                }
                else{
                    dispatch(getProductsCategory(data,'desc',begin,end,page))
                    setSort('desc');
                }
                break;
            default:
                if (title === 'search') {
                    dispatch(getProductsSearch(data,'',begin,end,page))
                    setSort('');
                }
                else{
                    dispatch(getProductsCategory(data,'',begin,end,page))
                    setSort('');
                }
            break;
           
        }
    }

    useEffect(() => {
        setIsLoading(false)
        setNextPage(false)
        setPage(1) 
        switch (title) {
            case 'search':
                dispatch(getProductsSearch(data,'','','',page));
                break;
            default:
                dispatch(getProductsCategory(data,'','','', page));
                break;
        }  
    }, [data,title])

    useEffect(() => {
        switch (title) {
            case 'search':
                if (productSearch.data) {
                    setProducts(productSearch.data);
                }
                else{
                    setProducts([]);
                }
                setNextPage(productSearch.isload)
                break;
            default:
                if (productCategory.data) {
                    setProducts(productCategory.data);   
                }
                else{
                    setProducts([]);
                }
                setNextPage(productSearch.isload)
                break;
        }
        if (products.length) {
            setisLoadMore(false);
            setIsLoading(true) 
        }
       
    }, [productSearch,productCategory])


    const getNewPage = () =>{  
        setPage(page + 1);  
    }

    return (
        <>
            <HeaderTop />
                <div className="search-container">
                    <div className="search-container-con">
                        <div className="sort-filter">
                            <div className="search-container-sort">
                                <div>Filter : </div>
                                <select className="select-sort" onChange={(e: any) => filterPrice(+e.currentTarget.value)}>
                                    <option value={0}>All</option>
                                    <option value={1}> &lt; 1.000.000đ </option>
                                    <option value={2}> 1.000.000đ - 10.000.000đ </option>
                                    <option value={3}> 	&gt; 10.000.000đ </option>
                                </select>
                            </div>
                            <div className="search-container-filter">
                                <div>Sort : </div>
                                <select className="select-sort" onChange={(e: any) => sortName(+e.currentTarget.value)}>
                                    <option value={0}>All</option>
                                    <option value={1}>A - Z</option>
                                    <option value={2}>Z - A</option>
                                </select>
                            </div>
                        </div>
                        <div className="product-in-search">
                            <div className="home-container">      
                            {
                                isLoading ?    
                                    <div className="home-container-product">
                                        {products.map((item:ProductModel, index:number) => <Product product={item} key={index} />)}
                                    </div> 
                                :
                                <Loading type="screen"/>
                            }   
                            </div>
                        </div>
                        {
                            !isLoadMore ?
                            (products.length !==0 && nextPage) &&
                            <div onClick={getNewPage} className="product-load-new">         
                                Xem thêm . . .              
                            </div>   
                            : 
                            <div className="categories-load-more">
                                <Loading type="max" />   
                            </div>
                        }
                            
                       
                    </div>
                </div>
            <Footer />
        </>
    )
}
