import Footer from '../../components/footer/Footer';
import HeaderTop from '../../components/header/HeaderTop';
import './home.css'
import Category from './../../components/category/Category';
import Slider from "react-slick";
import { useState, useRef, useEffect } from 'react';
import Product from '../../components/Product/Product';
import SlideItem from '../../components/slideItem/SlideItem';
import { ArrowBackIosOutlined, ArrowForwardIosOutlined } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CategoryModel, CategoryState, getCategory, ProductState, getProductsCategory, getProductsHot, getProductsNew, getSlider, SliderState, State, ProductModel, ProductDataPage, SliderModel } from '../../redux';
import Loading from '../../components/loading/Loading';


export default function Home() {
  const sliderRef = useRef<any>();
  const categoriesSlider = useRef<any>();
  const [catergoryIndex, setCategoryIndex] = useState(0);
  const [categoryLoad, setCategoryLoad] = useState(true);
  const productState: ProductState = useSelector((state: State) => state.productReducer);
  const dispatch = useDispatch();
  const sliderState: SliderState = useSelector((state: State) => state.sliderReducer);
  const slider:SliderModel[] = sliderState.slider;
  const categoriesState: CategoryState = useSelector((state: State) => state.categoryReducer);
  const categories: CategoryModel[] = categoriesState.categories ? categoriesState.categories : [];
  const [product, setProduct] = useState<any>()
  const { productHot, productNew, productCategory }: { productHot: ProductModel[], productNew: ProductModel[], productCategory: ProductDataPage } = productState;

  useEffect(() => {
    if (categories.length !== 0) {
      dispatch(getProductsCategory(categories[0].category_id));
    }
  }, [categories])

  useEffect(() => {
    setProduct(productCategory)
    if (product) {
      setCategoryLoad(false)
    }
  }, [productCategory])

  const onTabGetCatPro = (id: any, index: any) => {
    setCategoryLoad(true)
    setCategoryIndex(index)
    dispatch(getProductsCategory(id));
  }

  function next(refSlider: any) {
    refSlider.current.slickNext();
  };

  function previous(refSlider: any) {
    refSlider.current.slickPrev();
  };

  useEffect(() => {
    window.scrollTo(0, 0)
    dispatch(getProductsHot());
    dispatch(getProductsNew());
    dispatch(getCategory());
    dispatch(getSlider());
  }, []);

  return (
    <>
      <HeaderTop />
      <div className="conatiner-head">
        <div className="home-container">
          <div className="home-container-con">
            <div className="home-banner-left">
              <button className="btn-banner btn-prev-banner" onClick={() => previous(sliderRef)}> <ArrowBackIosOutlined style={{ color: "white" }} /> </button>
              <Slider ref={sliderRef} className="aaaa" {...settings1}>
                {
                  slider &&
                  slider.map((item: SliderModel, index: any) => <SlideItem key={index} slider={item} />)
                }
              </Slider>
              <button className="btn-banner btn-next-banner" onClick={() => next(sliderRef)}> <ArrowForwardIosOutlined style={{ color: "white" }} /> </button>
            </div>
            <div className="home-banner-right">
              <div className="home-banner-right-top">
                <img src={ slider?.length ? slider[0].slider_image : ""} alt={ slider?.length ? slider[0].slider_image : ""} />
              </div>
              <div className="home-banner-right-bottom">
                <img src={ slider?.length ? slider[1].slider_image : ""} alt={ slider?.length ? slider[1].slider_image : ""} />
              </div>
            </div>
          </div>
        </div>

        {/* Mang categories */}
        <div className="home-container">
          <div className="home-container-categories">
            <button className="btn-categories btn-prev-categories" onClick={() => previous(categoriesSlider)}><ArrowBackIosOutlined style={{ color: "black", fontSize: 21 }} /></button>
            <Slider ref={categoriesSlider}  {...settings}>
              {
                categories ?
                  categories.map((item: any, index: any) => <Category type='char' category={item} onTab={() => onTabGetCatPro(item.category_id, index)} index={index} activeIndex={catergoryIndex} key={index} />)
                  :
                  <Loading />
              }

              <Link className="categories-links" to='/categories'>Xem thêm . . .</Link>
            </Slider>

            <button className="btn-categories btn-next-categories" onClick={() => next(categoriesSlider)}><ArrowForwardIosOutlined style={{ color: "black", fontSize: 20 }} /></button>
          </div>

        </div>

        {/* Mang product */}
        <div className="home-container">
          <div className="home-container-product">
            {
                !categoryLoad ?
                productCategory.data  &&
                productCategory.data.map((item: ProductModel, index: number) => <Product product={item} key={index} />)
                :
                <Loading type="screen"/>
            }
          </div>
        </div>


        <div className="home-container">
          <div className="home-title">
            <div className="h1-home-title">Sản phẩm mới</div>
          </div>
        </div>

        <div className="home-container">
          <div className="home-container-product">
            {
              productNew ?
                productNew.map((item: ProductModel, index: number) => <Product product={item} key={index} />)
                :
                <Loading />
            }
          </div>
        </div>

        <div className="home-container">
          <div className="home-title">
            <div className="h1-home-title">Sản phẩm nổi bật</div>
          </div>
        </div>

        <div className="home-container">
          <div className="home-container-product">
            {
              productHot ?
                productHot.map((item: ProductModel, index: number) => <Product product={item} key={index} />)
                :
                <Loading />
            }
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}
const settings1 = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
  arrows: false
};
const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 8,
  slidesToScroll: 4,
  initialSlide: 0,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        initialSlide: 0
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    }
  ]
};