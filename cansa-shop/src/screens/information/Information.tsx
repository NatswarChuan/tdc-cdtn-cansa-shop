import React, { useEffect, useState } from 'react'
import { Col, Form, Row } from 'react-bootstrap'
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import HeaderTop from '../../components/header/HeaderTop'
import 'bootstrap/dist/css/bootstrap.min.css';
import './information.css'
import moment from 'moment';
import { Link } from 'react-router-dom';
import Footer from '../../components/footer/Footer';
import { getUserInfo } from '../../redux/actions/userActions'
import { State, UserModel, UserStage } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../../components/loading/Loading';
import IsLogin from '../../components/IsLogin';



export default function Information() {
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const [isLoading, setIsLoading] = useState(false);
    const { check, userInfor, updateUser }: { check: boolean, userInfor: UserModel, updateUser: number } = userState;
    const dispatch = useDispatch();
    const [birthday, setBirthday] = useState<string>('');

    useEffect(() => {
        dispatch(getUserInfo());
    }, [check, updateUser])


    useEffect(() => {
        dispatch(getUserInfo());
    }, [])

    useEffect(() => {
        setBirthday(moment.utc(userInfor.user_birthday + 86400000).format('DD-MM-YYYY'))
        setIsLoading(true);
    }, [userInfor])

    useEffect(() => {
    }, [birthday])
    return (

        <>
            <IsLogin />
            <HeaderTop></HeaderTop>
            {
                isLoading ?
                    <div className="infor_container">
                        <div className="container_con_infor">
                            {/* Xem thông tin cá nhân */}
                            <div className="title">
                                <Link className="link_backInfor" to="/account">
                                    <IconButton className="iconPre">
                                        <ArrowBackIcon></ArrowBackIcon>
                                        <h3>Thông Tin Cá Nhân</h3>
                                    </IconButton>
                                </Link>

                            </div>
                            <div className="inforAll">
                                <Form>
                                    <Form.Group as={Row} className="infor">
                                        <Form.Label column md="6" className="tag">
                                            <h5>Họ tên</h5>
                                        </Form.Label>
                                        <Col md="6">
                                            <Form.Control className="name" plaintext readOnly value={userInfor.user_real_name} />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} className="infor">
                                        <Form.Label column md="6" className="tag">
                                            <h5>Địa chỉ Email</h5>
                                        </Form.Label>
                                        <Col md="6">
                                            <Form.Control className="name" plaintext readOnly defaultValue={userInfor.user_email} />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} className="infor">
                                        <Form.Label column md="6" className="tag">
                                            <h5>Số điện thoại</h5>
                                        </Form.Label>
                                        <Col md="6" >
                                            <Form.Control className="name" plaintext readOnly value={userInfor.user_phone} />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} className="infor">
                                        <Form.Label column md="6" className="tag">
                                            <h5>Ngày sinh</h5>
                                        </Form.Label>
                                        <Col md="6">
                                            <Form.Control className="name" plaintext readOnly value={birthday} />
                                        </Col>
                                    </Form.Group>
                                </Form>

                                <div className="btnInfor">
                                    <button className="btn_information">
                                        <Link className="linkInfor" to="/updateinformation"> SỬA THÔNG TIN </Link>
                                    </button>

                                    <button className="btn_information">
                                        <Link className="linkInfor" to=""> THAY ĐỔI MẬT KHẨU </Link>
                                    </button>
                                </div>
                            </div >
                        </div>
                    </div>
                    :
                    <div className="productDetail-load">
                        <Loading />
                    </div>
            }
            <Footer></Footer>
        </>
    )


    //         {/* <div className="form-group">
    //                 Password:
    //                 <input type="password" name="passwd" onChange={(e: any) => checkValuePass(e)} className="form-control" placeholder="Nhập password" />
    //             </div>

    //             <div className="form-group">
    //                 Confirm Password:
    //                 <input type="password" name="confirmpasswd" onChange={(e: any) => checkValuCorfirm(e)} className="form-control" placeholder="Nhập confirm password" />
    //             </div>
    //             <input type="submit" className="btn-success" value="submit" />

    //             <div>
    //                 {isError}
    //             </div> */}

    //     </>

    // )

}
