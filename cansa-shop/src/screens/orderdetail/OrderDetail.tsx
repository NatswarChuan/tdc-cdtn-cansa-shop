import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router'
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import { getOder, OderModel, OderItemModel, OderState, State, updateOderItem, addComplaint } from '../../redux'
import './orderdetail.css'
import { TextToSlug, vnd } from '../../consts/Selector'
import moment from 'moment'
import Loading from '../../components/loading/Loading'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import IsLogin from '../../components/IsLogin'

export default function OrderDetail() {
    let { oder_id } = useParams<any>();
    const orderState: OderState = useSelector((state: State) => state.oderReducer);
    const [isLoading, setIsLoading] = useState(false);
    const [isComplaint, setIsComplaint] = useState(false);
    const [complaint, setComplaint] = useState('');
    const { oder }: { oder: OderModel } = orderState;
    const [openDelete, setOpenDelete] = React.useState(false)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getOder(oder_id))
    }, [oder_id])

    useEffect(() => {
        if (oder) {
            setIsLoading(true);
        }
    }, [orderState])

    const getTotalPrice = () => {
        let total = 0;
        oder.product_oder && oder.product_oder.map((product: OderItemModel) => {
            total += (product.product.product_price * (100 - product.product.product_sale) / 100) * product.product_quantity;
        })
        return total;
    }

    const handleClickOpenDelete = () => {
        setOpenDelete(true);
    }

    function handleCloseDelete(oder_id: number = -1, product_id: number = -1) {
        if (oder_id !== -1) {
            dispatch(updateOderItem(product_id, oder_id))
        }
        setOpenDelete(false)
    }

    const sendComplaint = (product_id: number) => {
        dispatch(addComplaint(product_id, complaint));
        setIsComplaint(false);
    }

    return (
        <>
            <IsLogin />
            <HeaderTop></HeaderTop>
            {
                isLoading ?
                    <div className="orderdetail_container">
                        <div className="container_con_orderdetail">
                            <div className="title_orderdetail">
                                Chi tiết đơn hàng
                            </div>
                            {/* Chi tiết sản phẩm */}
                            <div className="list_orderdetail">
                                <div className="name_shop_detail">
                                    <div className="orderdetail_key_time">
                                        <div className="orderdetail_key">
                                            <span className="title_key" id="key_and_time">Mã đơn hàng</span>
                                            <span className="key_all">{oder.oder_id}</span>
                                        </div>
                                        <div className="orderdetail_time">
                                            <span className="title_time" id="key_and_time">Ngày đặt hàng</span>
                                            <span className="time_all">{moment(oder.oder_date).format('DD/MM/YYYY')}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="title_row_orderdetail">
                                    {
                                        oder.product_oder && oder.product_oder.map((product: OderItemModel, index) => {
                                            return (
                                                <Row className="list_product_detail" key={index}>
                                                    <Col md={4}>
                                                        <Link to={`/product-detail/${TextToSlug(product.product.product_title)}/${product.product.product_id}`}>
                                                            <div className="order_img_product_detail">
                                                                <img src={product.product.product_avatar} />
                                                            </div>
                                                        </Link>

                                                    </Col>
                                                    <Col md={4}>
                                                        <div className="order_price_product_detail">
                                                            {vnd((product.product.product_price * (100 - product.product.product_sale) / 100) * product.product_quantity)}đ
                                                        </div>
                                                    </Col>
                                                    <Col md={3}>
                                                        Qty: {product.product_quantity}
                                                    </Col>
                                                    <Col md={1}>
                                                        {
                                                            (product.status === 1) ?
                                                                (
                                                                    <button className="orderdetail_btn_delete" onClick={handleClickOpenDelete}>
                                                                        Xoá
                                                                    </button>
                                                                )
                                                                :
                                                                (product.status === 0) ?
                                                                    <div className="orderdetail_delete"> Đã hủy </div> :
                                                                    (product.status === 4) &&
                                                                    (
                                                                        <button className="orderdetail_btn_report" onClick={() => setIsComplaint(true)}>
                                                                            Báo Cáo
                                                                        </button>
                                                                    )
                                                        }
                                                        {
                                                            isComplaint &&
                                                            <>
                                                                <Dialog open={isComplaint} onClose={() => setIsComplaint(false)} aria-labelledby="form-dialog-title">
                                                                    <DialogTitle id="form-dialog-title">Báo Cáo</DialogTitle>
                                                                    <DialogContent>
                                                                        <DialogContentText>
                                                                            Hãy viết báo cáo về chất lượng sản phẩm {product.product.product_title}
                                                                        </DialogContentText>
                                                                        <input type="text" onChange={(e: any) => setComplaint(e.target.value)} className="oderedetail_textarea" placeholder="Báo cáo của bạn....." />
                                                                    </DialogContent>
                                                                    <DialogActions>
                                                                        <Button onClick={() => setIsComplaint(false)} color="primary">
                                                                            Huỷ
                                                                        </Button>
                                                                        <Button onClick={() => sendComplaint(product.product.product_id)} color="primary">
                                                                            Gửi
                                                                        </Button>
                                                                    </DialogActions>
                                                                </Dialog>
                                                            </>
                                                        }
                                                        <Dialog
                                                            open={openDelete}
                                                            onClose={() => handleCloseDelete()}
                                                            aria-labelledby="draggable-dialog-title"
                                                        >
                                                            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                                                                Lưu ý
                                                            </DialogTitle>
                                                            <DialogContent>
                                                                <DialogContentText>
                                                                    Bạn có chắc chắn muốn huỷ đơn hàng này không?
                                                                </DialogContentText>
                                                            </DialogContent>
                                                            <DialogActions>
                                                                <Button id="ordered_btn_in_update" autoFocus onClick={() => handleCloseDelete()} >
                                                                    Hủy
                                                                </Button>
                                                                <Button id="ordered_btn_in_update_delete" onClick={() => handleCloseDelete(Number(oder.oder_id), Number(product.product.product_id))} >
                                                                    Xác nhận
                                                                </Button>
                                                            </DialogActions>
                                                        </Dialog>
                                                    </Col>
                                                </Row>
                                            )
                                        })
                                    }

                                </div>
                            </div>

                            {/* Thông tin khách hàng và giá */}
                            <div className="orderdetail_infor">
                                <Row>
                                    <Col md={6} className="orderdetail_col_price">
                                        <div className="orderdetail_title_order">
                                            <h5>Thông tin đơn hàng</h5>
                                        </div>
                                        <Row>
                                            <Col md={6}>
                                                <div className="orderdetail_title_price_product" id="orderdetail_title_ic">Tổng giá đơn hàng</div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="orderdetail_price_all_product" id="orderdetail_price_id">{vnd(getTotalPrice())}</div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={6}>
                                                <div className="orderdetail_title_price_ship" id="orderdetail_title_ic">Phí giao hàng</div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="orderdetail_price_ship" id="orderdetail_price_id">20.000</div>
                                            </Col>
                                        </Row>
                                        <hr />
                                        <Row>
                                            <Col md={6}>
                                                <div className="orderdetail_title_price_all">Tổng cộng</div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="orderdetail_price_all" id="orderdetail_price_id">{vnd(getTotalPrice() + 20000)}</div>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="productDetail-load">
                        <Loading />
                    </div>
            }

            <Footer></Footer>
        </>
    )
}
