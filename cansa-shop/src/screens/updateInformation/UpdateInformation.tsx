import Button from '@material-ui/core/Button'
import { Col, Form, Row } from 'react-bootstrap'
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import HeaderTop from '../../components/header/HeaderTop'
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import './updateInformation.css'
import moment from 'moment';
import Footer from '../../components/footer/Footer';
import { Link } from 'react-router-dom';
import EditIcon from '@material-ui/icons/Edit';
import { getUserInfo } from '../../redux/actions/userActions'
import { ImageId, State, updateUserProfile, updateUserProfileDefault, UserModel, UserStage } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';
import { saveImage, updateImage } from '../../consts/Selector';
import { useHistory } from "react-router-dom";
import IsLogin from '../../components/IsLogin';

export default function UpdateInformation() {
    // Button lưu lại
    let img: ImageId = { id: 0 };
    const [open, setOpen] = React.useState(false)
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, userInfor, updateUser }: { check: boolean, userInfor: UserModel, updateUser: number } = userState;
    const dispatch = useDispatch();
    const [name, setname] = useState<string>("");
    const [phone, setphone] = useState<string>("");
    const [birthday, setbirthday] = useState<Date>(new Date());
    const [avatar, setavatar] = useState<number | undefined>(undefined);
    const [file, setFile] = useState<File>();
    const [image, setimage] = useState<string>("/3.3.jpg");
    const history = useHistory();
    const [checkSave, setcheckSave] = useState<boolean>(false);
    const handleClickOpen = () => {
        setOpen(true);
    }

    function handleClose() {
        window.history.go(-1);
        setOpen(false)
    }


    function handleSubmitForm(e: any) {
        e.defaultValue = { name };
        setOpen(false);
    }

    function handleChange(e: any) {
        setname(e.target.value);
    }

    // lấy thông tin user từ api


    useEffect(() => {
        if (!check) {
            history.push("/");
        }
        dispatch(getUserInfo());
    }, [check])

    useEffect(() => {
        if (userInfor) {
            setname(userInfor.user_real_name);
            setphone(userInfor.user_phone);
            setbirthday(new Date(userInfor.user_birthday + 86400000));
            setavatar(Number(userInfor.user_avatar));
            setimage(userInfor.user_avatar_image);
        }
    }, [userInfor])

    useEffect(() => {
        if (checkSave) {
            dispatch(getUserInfo());
            setcheckSave(false);
            history.push('/information');
        }
    }, [updateUser]);



    useEffect(() => {
        if (avatar && checkSave) {
            dispatch(updateUserProfile(name, phone, birthday, avatar));
        }
    }, [avatar])


    function save() {
        if (/(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(phone) && name && birthday) {
            if (userInfor.user_avatar && file) {
                //Sửa hình
                updateImage(file, Number(userInfor.user_avatar), img).then(() => {
                    setcheckSave(true);
                    setavatar(img.id);
                })
            } else if (file) {
                //Thêm hình
                saveImage(file, img).then(() => {
                    setcheckSave(true);
                    setavatar(img.id);
                })
            }
            else {
                dispatch(updateUserProfileDefault(false));
                setcheckSave(true);
                dispatch(updateUserProfile(name, phone, birthday, Number(userInfor.user_avatar)));
            }
        }

    }


    const getImage = (e: any) => {
        setFile(e.target.files[0]);
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onloadend = function (e: any) {
            reader.result && setimage(reader.result.toString());
        };
    }

    return (
        <>
            <HeaderTop></HeaderTop>

            <div className="updateinfor_containers">
                <div className="container_con_update">
                    {/* Sửa thông tin cá nhân */}
                    <div className="title">
                        <Link className="link_backUpdateInfor" to="/information">
                            <IconButton className="iconPre">
                                <ArrowBackIcon></ArrowBackIcon>
                                <h3>Chỉnh Sửa Thông Tin</h3>
                            </IconButton>
                        </Link>
                    </div>
                    <div className="imageAvatar_update">
                        <div className="avatarAccount_update">
                            <div className="image_update">
                                <img src={image}>
                                </img>
                            </div>
                            <div className="update_fileIMG">
                                <input id="icon-button-file" type="file" onChange={(e: any) => getImage(e)} />
                                <label className="btn_icon_edit" htmlFor="icon-button-file">
                                    <IconButton color="primary" aria-label="upload picture" component="span">
                                        <EditIcon className="update_icon_edit" />
                                    </IconButton>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="inforAll">
                        <Form>
                            {/* <Form.Group as={Row} className="infor">
                                <Form.Label column md="2" className="tag">
                                    <h5>Avatar</h5>
                                </Form.Label>
                                <Col md="10">
                                    <div className="update_fileIMG">
                                        <input
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                        />
                                        <label htmlFor="contained-button-file">
                                            <Button className="btn_update" variant="contained" color="primary" component="span">
                                                Upload
                                            </Button>
                                        </label>
                                    </div>
                                </Col>
                            </Form.Group> */}
                            <Form.Group as={Row} className="infor">
                                <Form.Label column md="2" className="tag">
                                    <h5>Họ tên</h5>
                                </Form.Label>
                                <Col md="10">

                                    <Form.Control className="name" value={name} onChange={(e: any) => setname(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="infor">
                                <Form.Label column md="2" className="tag">
                                    <h5>Địa chỉ Email</h5>
                                </Form.Label>
                                <Col md="10">
                                    <Form.Control className="name" plaintext readOnly defaultValue={userInfor.user_email} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="infor">
                                <Form.Label column md="2" className="tag">
                                    <h5>Số điện thoại</h5>
                                </Form.Label>
                                <Col md="10">
                                    <Form.Control className="name" value={phone} onChange={(e: any) => setphone(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="infor">
                                <Form.Label column md="2" className="tag">
                                    <h5>Ngày sinh</h5>
                                </Form.Label>
                                <Col md="10">
                                    <Form.Control
                                        className="name"
                                        id="date"
                                        type="date"
                                        value={moment.utc(birthday).format('YYYY-MM-DD')}
                                        onChange={(e: any) => setbirthday(e.target.value)}
                                    />
                                </Col>
                            </Form.Group>
                        </Form>

                        <div className="btnInfor">
                            <button className="update_btn_save" onClick={() => handleClickOpen()}>
                                Lưu Lại
                            </button>
                            <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="draggable-dialog-title"
                            >
                                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                                    Lưu ý
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        Bạn có chắc chắn muốn lưu về thay đổi của mình không?
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button autoFocus onClick={handleClose} color="primary">
                                        Cancel
                                    </Button>
                                    <Button onClick={(e: any) => save()} color="primary">
                                        Save
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </div >
                </div>
            </div>

            <Footer></Footer>
        </>
    )

}
