import axios from "axios";
import { ImageId } from "../redux";

/*
 * str: chuoi cat
 * maxlimit: do dai toi da, cat - 3 ki tu
 */
export function SlugStr(str: string, maxlimit: number): string {
    if (str.length >= maxlimit) {
        return str.substring(0, maxlimit - 3);
    }
    return str
}

export function SlugStrTitle(str: string, maxlimit: number): string {
    if (str.length >= maxlimit) {
        return str.substring(0, maxlimit - 3) + "...";
    }
    return str
}

export function TextToSlug(str:string)
{
    str = str.toLowerCase();     
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
    str = str.replace(/(đ)/g, 'd');
    str = str.replace(/([^0-9a-z-\s])/g, '');
    str = str.replace(/(\s+)/g, '-');
    str = str.replace(/^-+/g, '');
    str = str.replace(/-+$/g, '');
    return str;
}

export function saveImage(img: any, obj: ImageId) {
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data;'
        }
    };
    var data = new FormData();
    data.append('file', img);
    return axios.post(`${cansa[0]}/api/image/save/e4611a028c71342a5b083d2cbf59c494`, data, config).then(
        (resp: any) => {
            const avatar_user = resp.data.data.image_id;
            obj.id = avatar_user;
        });
}

export function updateImage(img: any, id_avatar: number, obj: ImageId) {
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data;'
        }
    };
    var data = new FormData();
    data.append('file', img);
    return axios.post(`${cansa[0]}/api/image/update/${id_avatar}/e4611a028c71342a5b083d2cbf59c494`, data, config).then(
        (resp: any) => {
            const avatar_user = resp.data.data.image_id;
            obj.id = avatar_user;
        });
}

export const cansa =
    ["https://103.207.38.200:333",
        "https://103.207.38.200:443"];

export function vnd(n: number | string) {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export const chatSever = 'https://103.207.38.200:4320';
